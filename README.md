# Scout RFP homework

## Usage

- Run either all tests by running **testHousingSorting.feature** or by running a scenario from the file
- Run the tests using Gradle from cli when in project directory

**Unix**

```bash
./gradlew clean test
```

## Reporting
To generate report after test run, use

```bash
./gradlew clean test allureReport
```
and to view the report in browser

```bash
./gradlew allureServe
```

![](images/allure_report.png)