package com.scout.web.at.config

import com.codeborne.selenide.Configuration

class TestConfig {
    protected void configure() {
        Configuration.browser = "config.ChromeWebDriver"
    }
}
