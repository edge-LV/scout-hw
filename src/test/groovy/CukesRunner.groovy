import cucumber.api.CucumberOptions
import cucumber.api.junit.Cucumber
import org.junit.runner.RunWith

@RunWith(Cucumber.class)
@CucumberOptions(
        features = 'classpath:features',
        glue = ['classpath:com.scout.web.at'],
        plugin = [
                "pretty",
                "io.qameta.allure.cucumber4jvm.AllureCucumber4Jvm"])
class CukesRunner {
}

