package com.scout.web.at.steps

import com.scout.web.at.pages.MainPage
import com.scout.web.at.utils.PageUrls
import cucumber.api.java.en.Given

import javax.inject.Inject

import static com.codeborne.selenide.Selenide.open

class MainPageStepdefs {

    @Inject
    MainPage mainPage
    @Inject
    PageUrls pageUrls

    @Given('^user opens main page in english$')
    void user_opens_main_page() {
        open(pageUrls.mainPageUrl)
    }

    @Given('^user opens housing page$')
    void user_opens_housing_page() {
        mainPage.openHousingPage()
    }
}
