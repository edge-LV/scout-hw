package com.scout.web.at

import cucumber.api.java.After

import static com.codeborne.selenide.WebDriverRunner.closeWebDriver

class Hooks {

    @After
    void afterScenario() {
        closeWebDriver()
    }
}
