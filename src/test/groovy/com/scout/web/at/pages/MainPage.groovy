package com.scout.web.at.pages

import org.openqa.selenium.By

import static com.codeborne.selenide.Condition.visible
import static com.codeborne.selenide.Selenide.$

class MainPage extends Page {


    private def pageCountry = $(By.xpath("//h2[text() = 'Finland']"))
    private def housingPage = $('[data-cat="hhh"]')

    @Override
    void expectPageElements() {
        pageCountry.shouldBe(visible)
        housingPage.shouldBe(visible)
    }

    void openHousingPage() {
        housingPage.click()
    }


}
