@allWeb @housingSorting
Feature: Housing sorting

  Background:
    Given user opens main page in english
    And user opens housing page

  Scenario: Check that default sorting possibilities are available
    When user opens sort dropdown
    Then sort dropwdown values are:
      | newest  |
      | price ↑ |
      | price ↓ |

  Scenario: Check available sorting possibilities after using search
    When user uses search
    And user opens sort dropdown
    Then sort dropwdown values are:
      | upcoming |
      | newest   |
      | relevant |
      | price ↑  |
      | price ↓  |

  Scenario: Check ascending and descending sorting works
    When user opens sort dropdown
    And user sorts by price ↑
    Then housing items for given currencies are in ascending order:
      | SEK |
      | €   |
      | руб |
    When user opens sort dropdown
    And user sorts by price ↓
    Then housing items for given currencies are in descending order:
      | SEK |
      | €   |
      | руб |
